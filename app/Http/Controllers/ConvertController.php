<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class ConvertController extends Controller
{
    public function uppercase()
    {
        $method_convert = "Maiúscula";
        return view('site.uppercase.index', ['method_convert' => $method_convert]);
    }

    public function tiny()
    {
        $method_convert = "Minúsculo";
        return view('site.tiny.index', ['method_convert' => $method_convert]);
    }

    public function firstUppercase()
    {
        $method_convert = "Primeira letra maiúscula";
        return view('site.first_uppercase.index', ['method_convert' => $method_convert]);
    }

    public function hashtags()
    {
        $method_convert = "Adicionar Hashtags";
        return view('site.hashtags.index', ['method_convert' => $method_convert]);
    }

    public function repeat()
    {
        $method_convert = "Repetir";
        return view('site.repeat.index', ['method_convert' => $method_convert]);
    }
}
