<?php

use Illuminate\Support\Facades\Route;
use App\Http\Controllers\HomeController;
use App\Http\Controllers\ConvertController;
/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get("/", [HomeController::class, "index"]);
Route::group(['prefix' => 'convert'], function () 
{
    Route::get("/uppercase",                [ConvertController::class, "uppercase"]);
    Route::get("/tiny",                     [ConvertController::class, "tiny"]);
    Route::get("/first_uppercase",          [ConvertController::class, "firstUppercase"]);
    Route::get("/hashtags",                 [ConvertController::class, "hashtags"]);
    Route::get("/repeat",                   [ConvertController::class, "repeat"]);
});


