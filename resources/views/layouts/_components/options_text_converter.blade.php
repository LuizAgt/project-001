{{ $slot }}
<div class="container">
    <div class="row mt-5">
            <h3 class="mb-5">Veja todas as opções</h3>
            <div class="col">
                <a href="/convert/uppercase">
                    <div class="card-tool-text-p">
                        <img class="mb-4" src="{{ asset('images/icons/uppercase.svg') }}" alt="Cobverter para maiúsculo" />
                        <h3 class="text-light">Converter para maúsculo</h3>
                    </div>
                </a>
            </div>
            <div class="col">
                <a href="/convert/tiny">
                    <div class="card-tool-text-p">
                        <img class="mb-4" src="{{ asset('images/icons/lower.svg') }}" alt="Cobverter para minúsculo" />
                        <h3 class="text-green">Converter para minúsculo</h3>
                    </div>
                </a>
            </div>
            <div class="col">
                <a href="/convert/first_uppercase">
                    <div class="card-tool-text-p">
                       <img class="mb-4" src=" {{ asset('images/icons/first_uppercase.svg') }}" alt="Primeira letra maiúscula" />
                        <h3 class="text-deep-cove">Primeira letra maúscula</h3>
                    </div>
                </a>
            </div>
            <div class="col">
                <a href="/convert/hashtags">
                    <div class="card-tool-text-p">
                       <img class="mb-4" src="{{ asset('images/icons/hashtag-tool.svg') }}" alt="Adicionar hashtags" />
                        <h3 class="text-orange">Adicionar Hashtags</h3>
                    </div>
                </a>
            </div>
            <div class="col">
                <a href="/convert/repeat">
                    <div class="card-tool-text-p">
                       <img class="mb-4" src="{{ asset('images/icons/repeat.svg') }}" alt="Repetir texto" />
                        <h3>Repetidor de texto</h3>
                    </div>
                </a>
            </div>
        </div>
    </div>