{{ $slot }}

<section>
    <div class="container mt-5">
        <div class="row">
                <div class="col">
                    <h4> Aqui você digita seu texto✏️</h4>
                      <div class="form-group">
                        <textarea id="text" class="form-control" onkeyup="convert()" rows="12"></textarea>
                    </div>
                </div> 
                 <!--<img class="right_arrow" src="{{ asset('images/icons/right_arrow.svg') }}" alt="Seta para a direita" /> -->  
                <div class="col">
                    <h4> Aqui você vê o resultado✨</h4>
                      <div class="form-group">
                        <textarea id="result" class="form-control" rows="12"></textarea>
                    </div>
                </div>    
        </div>
        <div class="row">
            <div class="info-area">
                <p>Total de palavras: </p>
                <p>Total de caracteres: </p>
            </div>
        </div>
    </div>    
</section>

