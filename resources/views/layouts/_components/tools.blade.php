   {{ $slot }}
   </section>
       <div class="container">
        <div class="row mt-5">
            <div class="row">
                <h3 class="mb-5">Outras ferramentas para você <img class="down-arrow" src="{{ asset('images/down_arrow.svg') }}" alt="Seta para baixo" /></h3>
            </div>
            <div class="row">
                <div class="col">
                    <div class="card-tool-p">
                        <img class="icon-tool" src="{{ asset('images/icons/email.svg')}}" alt="email"/>
                        <h3>Extrair e-mail</h3> 
                    </div>
                </div>
                <div class="col">
                    <div class="card-tool-p">
                        <img class="icon-tool" src="{{ asset('images/icons/hashtag.svg')}}" alt="hashtag"/>
                        <h3>Adicionar Hashtags</h3> 
                    </div>
                </div>
                <div class="col">
                    <div class="card-tool-p">
                        <img class="icon-tool" src="{{ asset('images/icons/math-minus.svg')}}" alt="hashtag"/>
                        <h3>Remover duplicata</h3> 
                    </div>
                </div>
                <div class="col">
                    <div class="card-tool-p">
                        <img class="icon-tool" src="{{ asset('images/icons/search.svg')}}" alt="hashtag"/>
                        <h3>Encontrar palavra</h3> 
                    </div>
                </div>
                <div class="col">
                    <div class="card-tool-p">
                        <img class="icon-tool" src="{{ asset('images/icons/unlock.svg')}}" alt="hashtag"/>
                        <h3>Adicionar Hashtags</h3> 
                    </div>
                </div>
            </div>
            </div>
        </div>
    </section>