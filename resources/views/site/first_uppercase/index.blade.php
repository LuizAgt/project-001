@extends('layouts.main')

@section('title', 'Converter para maiúsculo')

@section('content')

 <div class="row">
    <div class="text-center">
        <div class="current_method"><h2>{{ $method_convert }}</h2></div>
    </div>
</div>

@component('layouts._components.panel_text_edit')
@endcomponent

@component('layouts._components.options_text_converter')
@endcomponent

@component('layouts._components.tools')
@endcomponent

@endsection

<script>

String.prototype.capitalize = function() {
    return this.replace(/(?:^|\s)\S/g, function(a) { return a.toUpperCase(); });
};

function convert() {
  let text = document.getElementById("text").value;
   document.getElementById("result").innerHTML = text.capitalize();
}

</script>