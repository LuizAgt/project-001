@extends('layouts.main')

@section('title', 'Repetir')

@section('content')

 <div class="row">
    <div class="text-center">
        <div class="current_method"><h2>{{ $method_convert }}</h2></div>
    </div>
</div>
@component('layouts._components.panel_text_edit')
@endcomponent
<div class="container">
    <div class="row">
         <input type="email" class="form-control" onkeyup="repeat()" id="number-repeat" aria-describedby="emailHelp" placeholder="Quantas vezes você quer repetir?">
    </div>
</div>
@component('layouts._components.options_text_converter')
@endcomponent

@component('layouts._components.tools')
@endcomponent

@endsection

<script>

alert(numberRepeat);
function repeat() {
  numberRepeat =  document.getElementById("number-repeat").value;
  let text = document.getElementById("text").value;
  document.getElementById("result").innerHTML = text.repeat(numberRepeat);
}
</script>