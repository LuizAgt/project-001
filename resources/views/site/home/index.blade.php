@extends('layouts.main')

@section('title', 'Página Principal')

@section('content')
   <section>
    <div class="container">
        <div class="row mt-5">
            <div class="col-sm-8">
                <div class="welcome-header pt-md-5">
                    <h1 class="mb-4"> Olá, seja bem-vindo(a) ao <b>Project001</b>😊</h1>
                    <p>Esse site foi desenvolvido para te ajudar a economizar tempo em tarefas 
                    que podem ser bem monótonas! Aqui você pode alterar o seu texto das mais diversas 
                    formas possíveis completamente de graça! 
                    Se você tiver algum elogio ou sugestão de melhoria clique no botão abaixo e me envie uma massagem. </p>
                    <button type="button" class="btn btn-dark mb-4 mt-3">Deixar feedback💙</button>
                </div>
            </div>
            <div class="col-sm-3">
                <img class="home-image justify-content-end" src="{{ asset('images/home_circle.png') }}" alt="Imagem principal" />
            </div>
        </div>
        @component('layouts._components.options_text_converter')
        @endcomponent

        @component('layouts._components.tools')
        @endcomponent
    </div>
@endsection